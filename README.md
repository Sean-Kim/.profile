# Sean Kim

Development Fund recipient primarily working in the Sculpt, Texture, Paint module.

---

* Devtalk - [Sean-Kim](https://devtalk.blender.org/u/sean-kim)
* RCS - [Sean-Kim](https://blender.community/Sean-Kim/)
* Github - [skimmedsquare](https://github.com/skimmedsquare)